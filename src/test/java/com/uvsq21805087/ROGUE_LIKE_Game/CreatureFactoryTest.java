package com.uvsq21805087.ROGUE_LIKE_Game;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Test;
import com.uvsq21805087.*;
import asciiPanel.AsciiPanel;

public class CreatureFactoryTest {
	World world 		= new WorldBuilder(20,20,5).build();
	Creature creature	= new Creature(world, '@', AsciiPanel.brightWhite, "player", 100, 5, 10);
	CreatureAi ai 		= new CreatureAi(creature);
	CreatureFactory cf	= new CreatureFactory(world);
	FieldOfView fov		= new FieldOfView(world);
	
	@Test(expected = NullPointerException.class) 
	public void testNewPlayer() {
		ArrayList<String> a = new ArrayList<String>();
		a.add("messages");
		assertNotNull(cf.newPlayer(a, null));
	}

	@Test(expected = NullPointerException.class) 
	public void testNewFruit() {
		Item fruit = cf.newFruit(1);
		assertNull(fruit);
	}

}
