package com.uvsq21805087.ROGUE_LIKE_Game;

import static org.junit.Assert.*;

import java.awt.Color;
import org.junit.Test;
import com.uvsq21805087.*;
import asciiPanel.AsciiPanel;

public class CreatureAiTest {

	World world 		= new WorldBuilder(20,20,5).build();
	Creature creature	= new Creature(world, '@', AsciiPanel.brightWhite, "player", 100, 5, 10);
	CreatureAi ai 		= new CreatureAi(creature);
	
	@Test
	public void testCreatureAi() {	
		assertNotNull(ai);
	}

	@Test
	public void testGetName() {
		assertSame(ai.getName(new Item('%', AsciiPanel.black, "name", "cool")), "cool");
	}

	@Test
	public void testCanSee() {
		assertNotNull(ai.canSee(1, 0, 1));
	}

	//@Test
	//public void testCanThrowAt() {
	//	assertNotNull(ai.canThrowAt(new Creature(world, 'X', AsciiPanel.white, "Other", 100, 5, 10)));
	//}

	@Test
	public void testCanUseBetterEquipment() {
		assertNotNull(ai.canUseBetterEquipment());
	}

}
