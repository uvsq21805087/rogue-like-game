package com.uvsq21805087.ROGUE_LIKE_Game;

import static org.junit.Assert.*;

import org.junit.Test;

import com.uvsq21805087.Creature;
import com.uvsq21805087.CreatureAi;
import com.uvsq21805087.CreatureFactory;
import com.uvsq21805087.FieldOfView;
import com.uvsq21805087.World;
import com.uvsq21805087.WorldBuilder;

import asciiPanel.AsciiPanel;

public class WorldBuilderTest {

	World world 		= new WorldBuilder(20,20,5).build();
	Creature creature	= new Creature(world, '@', AsciiPanel.brightWhite, "player", 100, 5, 10);
	CreatureAi ai 		= new CreatureAi(creature);
	CreatureFactory cf	= new CreatureFactory(world);
	FieldOfView fov		= new FieldOfView(world);
	
	@Test
	public void testBuild() {
		assertNotNull(world);
	}

}
