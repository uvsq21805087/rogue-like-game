package com.uvsq21805087.ROGUE_LIKE_Game;

import static org.junit.Assert.*;

import org.junit.Test;

import com.uvsq21805087.Creature;
import com.uvsq21805087.CreatureAi;
import com.uvsq21805087.World;
import com.uvsq21805087.WorldBuilder;

import asciiPanel.AsciiPanel;

public class CreatureTest {
	World world 		= new WorldBuilder(20,20,5).build();
	Creature creature	= new Creature(world, '@', AsciiPanel.brightWhite, "player", 100, 5, 10);
	CreatureAi ai 		= new CreatureAi(creature);

	@Test
	public void testColor() {
		assertNotNull(creature.color());
	}

	@Test
	public void testHp() {
		assertNotNull(creature.hp());
	}

	@Test
	public void testModifyAttackValue() {
		creature.modifyAttackValue(5);
		assertEquals(creature.attackValue(), 10);
	}

	@Test
	public void testInventory() {
		assertNotNull(creature.inventory());
	}

	@Test
	public void testIsPlayer() {
		assertTrue(creature.isPlayer());
	}

}
