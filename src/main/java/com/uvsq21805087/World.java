package com.uvsq21805087;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class World {
	private Tile[][][] tiles;
	private Item[][][] items;

	private int width;

	/**
	 * gives the world width
	 * 
	 * @return width
	 */
	public int width() {
		return width;
	}

	private int height;

	/**
	 * gives the height
	 * 
	 * @return the wolld hight
	 */
	public int height() {
		return height;
	}

	private int depth;

	/**
	 * gives the depth
	 * 
	 * @return the world depth
	 */
	public int depth() {
		return depth;
	}

	private ArrayList<Creature> creatures;

	/**
	 * constructor
	 * 
	 * @param tiles a table of tiles
	 */
	public World(Tile[][][] tiles) {
		this.tiles = tiles;
		this.width = tiles.length;
		this.height = tiles[0].length;
		this.depth = tiles[0][0].length;
		this.creatures = new ArrayList<Creature>();
		this.items = new Item[width][height][depth];
	}

	/**
	 * Save game
	 * 
	 */
	public void save() {
		try {
			FileOutputStream saveTiles = new FileOutputStream("SaveTiles.sav");
			FileOutputStream saveCreatures = new FileOutputStream("SaveCreatures.sav");
			FileOutputStream saveItems = new FileOutputStream("SaveItems.sav");

			ObjectOutputStream saveT = new ObjectOutputStream(saveTiles);
			ObjectOutputStream saveC = new ObjectOutputStream(saveCreatures);
			ObjectOutputStream saveI = new ObjectOutputStream(saveItems);
			
			String jsonT = "{\"tiles : \"";
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					for (int z = 0; z < depth; z++) {
						if (this.tiles[x][y][z] != null) {
							jsonT += "{"
									+ "\"color\": \""+this.tiles[x][y][z].color()+",\""
									+ "\"glyph\": \""+this.tiles[x][y][z].glyph()+",\""
									+ "\"desc\" : \""+this.tiles[x][y][z].details()+"\","
								+ "},";
						}
					}
				}
			}
			jsonT.substring(0, jsonT.length()-1);
			jsonT+="}";
			
			saveT.writeObject(jsonT);
			
			for (Creature creature : this.creatures) {
				saveC.writeObject(creatures.toString());
			}
			
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					for (int z = 0; z < depth; z++) {
						if (this.items[x][y][z] != null) {
							saveI.writeObject(this.items[x][y][z].toString());
						}
					}
				}
			}
			
			saveT.close();
			saveC.close();
			saveI.close();
			
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	
	/**
	 * 
	 * Load game
	 * 
	 */
	@SuppressWarnings("unchecked")
	public void load() {
		try {
			FileInputStream saveTiles = new FileInputStream("SaveTiles.sav");
			FileInputStream saveCreatures = new FileInputStream("SaveCreatures.sav");
			FileInputStream saveItems = new FileInputStream("SaveItems.sav");

			ObjectInputStream saveT = new ObjectInputStream(saveTiles);
			ObjectInputStream saveC = new ObjectInputStream(saveCreatures);
			ObjectInputStream saveI = new ObjectInputStream(saveItems);
			
			Item item = null;
			
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					for (int z = 0; z < depth; z++) {
						if (this.items[x][y][z] != null) {
							item = (Item)saveI.readObject();
						}
					}
				}
			}
			
			System.out.println("last item : "+item.name());

			saveT.close();
			saveC.close();
			saveI.close();
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	/**
	 * returns the creature is it is at position(x,y,z)
	 * 
	 * @param x the coordinate x
	 * @param y the coordinate y
	 * @param z the coordinate z
	 * @return the creature at the coordinate (x,y,z) or null
	 */
	public Creature creature(int x, int y, int z) {
		for (Creature c : creatures) {
			if (c.x == x && c.y == y && c.z == z)
				return c;
		}
		return null;
	}

	/**
	 * returns the tile at (x,y,z)
	 * 
	 * @param x coordinate x
	 * @param y coordinate y
	 * @param z coordinate z
	 * @return the tile at (x,y,z) if dimensions not exceeded
	 */
	public Tile tile(int x, int y, int z) {
		if (x < 0 || x >= width || y < 0 || y >= height || z < 0 || z >= depth)
			return Tile.BOUNDS;
		else
			return tiles[x][y][z];
	}

	/**
	 * returns the creature glyph with x y z coordinates
	 * 
	 * @param x coordinate x
	 * @param y coordinate y
	 * @param z coordinate z
	 * @return the glyph
	 */
	public char glyph(int x, int y, int z) {
		Creature creature = creature(x, y, z);
		if (creature != null)
			return creature.glyph();

		if (item(x, y, z) != null)
			return item(x, y, z).glyph();

		return tile(x, y, z).glyph();
	}

	/**
	 * returns the creature color in coordinates x,y,z
	 * 
	 * @param x coordinate x
	 * @param y coordinate y
	 * @param z coordinate z
	 * @return color
	 */
	public Color color(int x, int y, int z) {
		Creature creature = creature(x, y, z);
		if (creature != null)
			return creature.color();

		if (item(x, y, z) != null)
			return item(x, y, z).color();

		return tile(x, y, z).color();
	}

	/**
	 * dig the wall in coordinates x, y, z
	 * 
	 * @param x coordinate x
	 * @param y coordinate y
	 * @param z coordinate z
	 */
	public void dig(int x, int y, int z) {
		if (tile(x, y, z).isDiggable())
			tiles[x][y][z] = Tile.FLOOR;
	}

	/**
	 * add new creature at coordinates x,y,z
	 * 
	 * @param creature the creature
	 * @param z coordinate z
	 */
	public void addAtEmptyLocation(Creature creature, int z) {
		int x;
		int y;

		do {
			x = (int) (Math.random() * width);
			y = (int) (Math.random() * height);
		} while (!tile(x, y, z).isGround() || creature(x, y, z) != null);

		creature.x = x;
		creature.y = y;
		creature.z = z;
		creatures.add(creature);
	}

	/**
	 * updates the creature
	 */
	public void update() {
		ArrayList<Creature> toUpdate = new ArrayList<Creature>(creatures);
		for (Creature creature : toUpdate) {
			creature.update();
		}
	}

	/**
	 * remove creature
	 * 
	 * @param other the other creature
	 */
	public void remove(Creature other) {
		creatures.remove(other);
	}

	/**
	 * remove the item
	 * 
	 * @param item the tem to remove
	 */
	public void remove(Item item) {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				for (int z = 0; z < depth; z++) {
					if (items[x][y][z] == item) {
						items[x][y][z] = null;
						return;
					}
				}
			}
		}
	}

	/**
	 * returns the item at coordinates (x,y,z)
	 * 
	 * @param x coordinate x
	 * @param y coordinate y
	 * @param z coordinate z
	 * @return the item[x,y,z]
	 */
	public Item item(int x, int y, int z) {
		return items[x][y][z];
	}

	/**
	 * add new item in empty location
	 * 
	 * @param item the item
	 * @param depth the depth
	 */
	public void addAtEmptyLocation(Item item, int depth) {
		int x;
		int y;

		do {
			x = (int) (Math.random() * width);
			y = (int) (Math.random() * height);
		} while (!tile(x, y, depth).isGround() || item(x, y, depth) != null);

		items[x][y][depth] = item;
	}

	/**
	 * remove the item a (x,y,z)
	 * 
	 * @param x coordinate x
	 * @param y coordinate y
	 * @param z coordinate z
	 */
	public void remove(int x, int y, int z) {
		items[x][y][z] = null;
	}

	public boolean addAtEmptySpace(Item item, int x, int y, int z) {
		if (item == null)
			return true;

		ArrayList<Point> points = new ArrayList<Point>();
		ArrayList<Point> checked = new ArrayList<Point>();

		points.add(new Point(x, y, z));

		while (!points.isEmpty()) {
			Point p = points.remove(0);
			checked.add(p);

			if (!tile(p.x, p.y, p.z).isGround())
				continue;

			if (items[p.x][p.y][p.z] == null) {
				items[p.x][p.y][p.z] = item;
				Creature c = this.creature(p.x, p.y, p.z);
				if (c != null)
					c.notify("A %s lands between your feet.", c.nameOf(item));
				return true;
			} else {
				ArrayList<Point> neighbors = p.neighbors8();
				neighbors.removeAll(checked);
				points.addAll(neighbors);
			}
		}
		return false;
	}

	/**
	 * add new pet
	 * 
	 * @param pet the pet
	 */
	public void add(Creature pet) {
		creatures.add(pet);
	}
}