package com.uvsq21805087;

public class Spell {

	private String name;

	public String name() {
		return name;
	}

	private int manaCost;

	/**
	 * gives the manacost
	 * 
	 * @return manacost
	 */
	public int manaCost() {
		return manaCost;
	}

	private Effect effect;

	public Effect effect() {
		return new Effect(effect);
	}

	/**
	 * constructor
	 * 
	 * @param name     the name
	 * @param manaCost the cost
	 * @param effect   the effect
	 */
	public Spell(String name, int manaCost, Effect effect) {
		this.name = name;
		this.manaCost = manaCost;
		this.effect = effect;
	}
}
