package com.uvsq21805087;

public class FungusAi extends CreatureAi {

	private CreatureFactory factory;
	private int spreadcount;

	/**
	 * constructor
	 * 
	 * @param creature the creature
	 * @param factory  the factory
	 */
	public FungusAi(Creature creature, CreatureFactory factory) {
		super(creature);
		this.factory = factory;
	}

	/**
	 * update fungus position
	 */
	public void onUpdate() {
		if (spreadcount < 5 && Math.random() < 0.01)
			spread();
	}

	/**
	 * the creature spread generates messages spawn childs
	 */
	private void spread() {
		int x = creature.x + (int) (Math.random() * 11) - 5;
		int y = creature.y + (int) (Math.random() * 11) - 5;

		if (!creature.canEnter(x, y, creature.z))
			return;

		creature.doAction("spawn a child");

		Creature child = factory.newFungus(creature.z);
		child.x = x;
		child.y = y;
		child.z = creature.z;
		spreadcount++;
	}
}
