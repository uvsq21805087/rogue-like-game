package com.uvsq21805087;

public abstract class LevelUpOption {
	private String name;

	public String name() {
		return name;
	}

	/**
	 * constructor
	 * 
	 * @param name gives the levelUpOption name
	 */
	public LevelUpOption(String name) {
		this.name = name;
	}

	public abstract void invoke(Creature creature);
}