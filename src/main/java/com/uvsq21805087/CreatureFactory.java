package com.uvsq21805087;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import asciiPanel.AsciiPanel;

public class CreatureFactory {
	private World world;
	private Map<String, Color> potionColors;
	private ArrayList<String> potionAppearances;

	/**
	 * constructor
	 * 
	 * @param world world
	 */
	public CreatureFactory(World world) {
		this.world = world;
		setUpPotionAppearances();
	}

	/**
	 * fixes colors
	 */
	private void setUpPotionAppearances() {
		potionColors = new HashMap<String, Color>();
		potionColors.put("red potion", AsciiPanel.brightRed);
		potionColors.put("yellow potion", AsciiPanel.brightYellow);
		potionColors.put("green potion", AsciiPanel.brightGreen);
		potionColors.put("cyan potion", AsciiPanel.brightCyan);
		potionColors.put("blue potion", AsciiPanel.brightBlue);
		potionColors.put("magenta potion", AsciiPanel.brightMagenta);
		potionColors.put("dark potion", AsciiPanel.brightBlack);
		potionColors.put("grey potion", AsciiPanel.white);
		potionColors.put("light potion", AsciiPanel.brightWhite);

		potionAppearances = new ArrayList<String>(potionColors.keySet());
		Collections.shuffle(potionAppearances);
	}

	/**
	 * creates the player with the glyph '@'
	 * 
	 * @param messages the list of messages
	 * @param fov      a field of view
	 * @return the created player
	 */
	public Creature newPlayer(ArrayList<String> messages, FieldOfView fov) {
		Creature player = new Creature(world, '@', AsciiPanel.brightWhite, "player", 100, 20, 5);
		world.addAtEmptyLocation(player, 0);
		new PlayerAi(player, messages, fov);
		return player;
	}

	/**
	 * creates new fungus
	 * 
	 * @param depth the fungus depth
	 * @return the fungus
	 */
	public Creature newFungus(int depth) {
		Creature fungus = new Creature(world, 'f', AsciiPanel.green, "fungus", 10, 0, 0);
		world.addAtEmptyLocation(fungus, depth);
		new FungusAi(fungus, this);
		return fungus;
	}

	/**
	 * creates new bat
	 * 
	 * @param depth the bat depth
	 * @return the created bat
	 */
	public Creature newBat(int depth) {
		Creature bat = new Creature(world, 'b', AsciiPanel.brightYellow, "bat", 15, 5, 0);
		world.addAtEmptyLocation(bat, depth);
		new BatAi(bat);
		return bat;
	}

	/**
	 * creates new zombie
	 * 
	 * @param depth  the zombie depth
	 * @param player the player
	 * @return the created zombie
	 */
	public Creature newZombie(int depth, Creature player) {
		Creature zombie = new Creature(world, 'z', AsciiPanel.white, "zombie", 50, 10, 10);
		world.addAtEmptyLocation(zombie, depth);
		new ZombieAi(zombie, player);
		return zombie;
	}

	/**
	 * creates new rock
	 * 
	 * @param depth the rock depth
	 * @return the rock item
	 */
	public Item newRock(int depth) {
		Item rock = new Item(',', AsciiPanel.yellow, "rock", null);
		world.addAtEmptyLocation(rock, depth);
		return rock;
	}

	/**
	 * creates new victory item
	 * 
	 * @param depth the item depth
	 * @return the victory item
	 */
	public Item newVictoryItem(int depth) {
		Item item = new Item('*', AsciiPanel.brightWhite, "teddy bear", null);
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * creates new bread
	 * 
	 * @param depth the bread depth
	 * @return the bread item
	 */
	public Item newBread(int depth) {
		Item item = new Item('%', AsciiPanel.yellow, "bread", null);
		item.modifyFoodValue(200);
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * creates new fruits
	 * 
	 * @param depth the fruit depth
	 * @return the fruit item
	 */
	public Item newFruit(int depth) {
		Item item = new Item('%', AsciiPanel.brightRed, "apple", null);
		item.modifyFoodValue(100);
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * creates a new dagger
	 * 
	 * @param depth the dagger depth
	 * @return the dagger item
	 */
	public Item newDagger(int depth) {
		Item item = new Item(')', AsciiPanel.white, "dagger", null);
		item.modifyAttackValue(5);
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * create a sword
	 * 
	 * @param depth the sword depth
	 * @return the sword item
	 */
	public Item newSword(int depth) {
		Item item = new Item(')', AsciiPanel.brightWhite, "sword", null);
		item.modifyAttackValue(10);
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * create a new staff
	 * 
	 * @param depth the staff depth
	 * @return the staff item
	 */
	public Item newStaff(int depth) {
		Item item = new Item(')', AsciiPanel.yellow, "staff", null);
		item.modifyAttackValue(5);
		item.modifyDefenseValue(3);
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * creates new edible weapon
	 * 
	 * @param depth the weapon depth
	 * @return the weapon item
	 */
	public Item newEdibleWeapon(int depth) {
		Item item = new Item(')', AsciiPanel.yellow, "baguette", null);
		item.modifyAttackValue(3);
		item.modifyFoodValue(50);
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * creates new light armor
	 * 
	 * @param depth the armor depth
	 * @return the new armor item
	 */
	public Item newLightArmor(int depth) {
		Item item = new Item('[', AsciiPanel.green, "tunic", null);
		item.modifyDefenseValue(2);
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * create a medium armor
	 * 
	 * @param depth the armor depth
	 * @return the armor item
	 */
	public Item newMediumArmor(int depth) {
		Item item = new Item('[', AsciiPanel.white, "chainmail", null);
		item.modifyDefenseValue(4);
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * creates a new heavy armor
	 * 
	 * @param depth the armor depth
	 * @return the armor item
	 */
	public Item newHeavyArmor(int depth) {
		Item item = new Item('[', AsciiPanel.brightWhite, "platemail", null);
		item.modifyDefenseValue(6);
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * returns a light, a medium or a heavy armor
	 * 
	 * @param depth the armor depth
	 * @return one of the three armors
	 */
	public Item randomArmor(int depth) {
		switch ((int) (Math.random() * 3)) {
		case 0:
			return newLightArmor(depth);
		case 1:
			return newMediumArmor(depth);
		default:
			return newHeavyArmor(depth);
		}
	}

	/**
	 * creates a bow
	 * 
	 * @param depth the bow depth
	 * @return the bow item
	 */
	public Item newBow(int depth) {
		Item item = new Item(')', AsciiPanel.yellow, "bow", null);
		item.modifyAttackValue(1);
		item.modifyRangedAttackValue(5);
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * creates a random weapon
	 * 
	 * @param depth the weapon depth
	 * @return one of the t3 random weapons generated
	 */
	public Item randomWeapon(int depth) {
		switch ((int) (Math.random() * 3)) {
		case 0:
			return newDagger(depth);
		case 1:
			return newSword(depth);
		case 2:
			return newBow(depth);
		default:
			return newStaff(depth);
		}
	}

	/**
	 * create a new goblin
	 * 
	 * @param depth  the goblin depth
	 * @param player the player
	 * @return the goblin creature
	 */
	public Creature newGoblin(int depth, Creature player) {
		Creature goblin = new Creature(world, 'g', AsciiPanel.brightGreen, "goblin", 60, 10, 5);
		goblin.equip(randomWeapon(depth));
		goblin.equip(randomArmor(depth));
		world.addAtEmptyLocation(goblin, depth);
		new GoblinAi(goblin, player);
		return goblin;
	}

	/**
	 * creates new potion of health
	 * 
	 * @param depth the potion depth
	 * @return the potion item
	 */
	public Item newPotionOfHealth(int depth) {
		Item item = new Item('!', AsciiPanel.white, "health potion", null);
		item.setQuaffEffect(new Effect(1) {
			public void start(Creature creature) {
				if (creature.hp() == creature.maxHp())
					return;

				creature.modifyHp(15, "Killed by a health potion?");
				creature.doAction("look healthier");
			}
		});
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * creates new potion of poison
	 * 
	 * @param depth the potion depth
	 * @return the potion item
	 */
	public Item newPotionOfPoison(int depth) {
		Item item = new Item('!', AsciiPanel.white, "poison potion", null);
		item.setQuaffEffect(new Effect(20) {
			public void start(Creature creature) {
				creature.doAction("look sick");
			}

			public void update(Creature creature) {
				super.update(creature);
				creature.modifyHp(-1, "Killed by a health poison?");
			}
		});

		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * creates new potion of warrior
	 * 
	 * @param depth the potion depth
	 * @return the potion item
	 */
	public Item newPotionOfWarrior(int depth) {
		Item item = new Item('!', AsciiPanel.white, "warrior's potion", null);
		item.setQuaffEffect(new Effect(20) {
			public void start(Creature creature) {
				creature.modifyAttackValue(5);
				creature.modifyDefenseValue(5);
				creature.doAction("look stronger");
			}

			public void end(Creature creature) {
				creature.modifyAttackValue(-5);
				creature.modifyDefenseValue(-5);
				creature.doAction("look less strong");
			}
		});

		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * select a random potion
	 * 
	 * @param depth the potion depth
	 * @return one of the generated potions
	 */
	public Item randomPotion(int depth) {
		switch ((int) (Math.random() * 3)) {
		case 0:
			return newPotionOfHealth(depth);
		case 1:
			return newPotionOfPoison(depth);
		default:
			return newPotionOfWarrior(depth);
		}
	}

	/**
	 * creates new white mages spellbook
	 * 
	 * @param depth the mages depth
	 * @return the mages item
	 */
	public Item newWhiteMagesSpellbook(int depth) {
		Item item = new Item('+', AsciiPanel.brightWhite, "white mage's spellbook", null);
		item.addWrittenSpell("minor heal", 4, new Effect(1) {
			public void start(Creature creature) {
				if (creature.hp() == creature.maxHp())
					return;

				creature.modifyHp(20, "Killed by a minor heal spell?");
				creature.doAction("look healthier");
			}
		});

		item.addWrittenSpell("major heal", 8, new Effect(1) {
			public void start(Creature creature) {
				if (creature.hp() == creature.maxHp())
					return;

				creature.modifyHp(50, "Killed by a major heal spell?");
				creature.doAction("look healthier");
			}
		});

		item.addWrittenSpell("slow heal", 12, new Effect(50) {
			public void update(Creature creature) {
				super.update(creature);
				creature.modifyHp(2, "Killed by a slow heal spell?");
			}
		});

		item.addWrittenSpell("inner strength", 16, new Effect(50) {
			public void start(Creature creature) {
				creature.modifyAttackValue(2);
				creature.modifyDefenseValue(2);
				creature.modifyVisionRadius(1);
				creature.modifyRegenHpPer1000(10);
				creature.modifyRegenManaPer1000(-10);
				creature.doAction("seem to glow with inner strength");
			}

			public void update(Creature creature) {
				super.update(creature);
				if (Math.random() < 0.25)
					creature.modifyHp(1, "Killed by inner strength spell?");
			}

			public void end(Creature creature) {
				creature.modifyAttackValue(-2);
				creature.modifyDefenseValue(-2);
				creature.modifyVisionRadius(-1);
				creature.modifyRegenHpPer1000(-10);
				creature.modifyRegenManaPer1000(10);
			}
		});

		world.addAtEmptyLocation(item, depth);
		return item;
	}

	/**
	 * creates new blue mages spellbook
	 * 
	 * @param depth the mages depth
	 * @return the mages item
	 */
	public Item newBlueMagesSpellbook(int depth) {
		Item item = new Item('+', AsciiPanel.brightBlue, "blue mage's spellbook", null);

		item.addWrittenSpell("blood to mana", 1, new Effect(1) {
			public void start(Creature creature) {
				int amount = Math.min(creature.hp() - 1, creature.maxMana() - creature.mana());
				creature.modifyHp(-amount, "Killed by a blood to mana spell.");
				creature.modifyMana(amount);
			}
		});

		item.addWrittenSpell("blink", 6, new Effect(1) {
			public void start(Creature creature) {
				creature.doAction("fade out");

				int mx = 0;
				int my = 0;

				do {
					mx = (int) (Math.random() * 11) - 5;
					my = (int) (Math.random() * 11) - 5;
				} while (!creature.canEnter(creature.x + mx, creature.y + my, creature.z)
						&& creature.canSee(creature.x + mx, creature.y + my, creature.z));

				creature.moveBy(mx, my, 0);

				creature.doAction("fade in");
			}
		});

		item.addWrittenSpell("summon bats", 11, new Effect(1) {
			public void start(Creature creature) {
				for (int ox = -1; ox < 2; ox++) {
					for (int oy = -1; oy < 2; oy++) {
						int nx = creature.x + ox;
						int ny = creature.y + oy;
						if (ox == 0 && oy == 0 || creature.creature(nx, ny, creature.z) != null)
							continue;

						Creature bat = newBat(0);
						if (!bat.canEnter(nx, ny, creature.z)) {
							world.remove(bat);
							continue;
						}

						bat.x = nx;
						bat.y = ny;
						bat.z = creature.z;

						creature.summon(bat);
					}
				}
			}
		});

		item.addWrittenSpell("detect creatures", 16, new Effect(75) {
			public void start(Creature creature) {
				creature.doAction("look far off into the distance");
				creature.modifyDetectCreatures(1);
			}

			public void end(Creature creature) {
				creature.modifyDetectCreatures(-1);
			}
		});
		world.addAtEmptyLocation(item, depth);
		return item;
	}
}