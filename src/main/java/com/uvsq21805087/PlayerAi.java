package com.uvsq21805087;

import java.util.ArrayList;

public class PlayerAi extends CreatureAi {
	private ArrayList<String> messages;
	private FieldOfView fov;
	
	/**
	 * constructor
	 * @param creature the creature
	 * @param messages a list of messages
	 * @param fov the fieldOfView
	 */
	public PlayerAi(Creature creature, ArrayList<String> messages, FieldOfView fov) {
		super(creature);
		this.messages = messages;
		this.fov = fov;
	}
	
	/**
	 * let the player enter if he can 
	 * @param x coordinate x
	 * @param y coordinate y
	 * @param z coordinate z
	 * @param tile the tile
	 */
	public void onEnter(int x, int y, int z, Tile tile){
		if (tile.isGround()){
			creature.x = x;
			creature.y = y;
			creature.z = z;
		} else if (tile.isDiggable()) {
			creature.dig(x, y, z);
		}
	}
	
	public void onNotify(String message){
		messages.add(message);
	}
	
	@Override
	public boolean canSee(int wx, int wy, int wz) {
	    return fov.isVisible(wx, wy, wz);
	}
	
	@Override
	public void onGainLevel(){
		
	}
	
	public Tile rememberedTile(int wx, int wy, int wz) {
        return fov.tile(wx, wy, wz);
    }
}