package com.uvsq21805087;

public class BatAi extends CreatureAi {

	/**
	 * constructor
	 * 
	 * @param creature the creature
	 */
	public BatAi(Creature creature) {
		super(creature);
	}

	public void onUpdate() {
		wander();
		wander();
	}
}