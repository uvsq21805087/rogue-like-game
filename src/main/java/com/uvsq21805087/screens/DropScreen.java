package com.uvsq21805087.screens;

import com.uvsq21805087.Creature;
import com.uvsq21805087.Item;

public class DropScreen extends InventoryBasedScreen {

	/**
	 * constructor
	 * 
	 * @param player the player
	 */
	public DropScreen(Creature player) {
		super(player);
	}

	/**
	 * verb getter
	 */
	@Override
	protected String getVerb() {
		return "drop";
	}

	/**
	 * verify if an item is acceptable
	 * 
	 * @param item the item
	 */
	@Override
	protected boolean isAcceptable(Item item) {
		return true;
	}

	@Override
	protected Screen use(Item item) {
		player.drop(item);
		return null;
	}
}