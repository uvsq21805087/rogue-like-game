package com.uvsq21805087.screens;

import java.awt.event.KeyEvent;
import asciiPanel.AsciiPanel;

public class StartScreen implements Screen {

	/**
	 * displays the initial screen
	 */
	public void displayOutput(AsciiPanel terminal) {
		terminal.write("    ____                       ", 4, 1);
		terminal.write("   / __ \\____  ____ ___  _____ ", 4, 2);
		terminal.write("  / /_/ / __ \\/ __ `/ / / / _ \\", 4, 3);
		terminal.write(" / _, _/ /_/ / /_/ / /_/ /  __/", 4, 4);
		terminal.write("/_/ |_|\\____/\\__, /\\__,_/\\___/ ", 4, 5);
		terminal.write("            /____/             ", 4, 6);

		terminal.write("  ________                       ", 39, 4);
		terminal.write(" /  _____/_____    _____   ____  ", 39, 5);
		terminal.write("/   \\  ___\\__  \\  /     \\_/ __ \\ ", 39, 6);
		terminal.write("\\    \\_\\  \\/ __ \\|  Y Y  \\  ___/ ", 39, 7);
		terminal.write(" \\______  (____  |__|_|  /\\___  >", 39, 8);
		terminal.write("        \\/     \\/      \\/     \\/ ", 39, 9);

		terminal.writeCenter("-- press [enter] to start --", 15);
	}

	public Screen respondToUserInput(KeyEvent key) {
		return key.getKeyCode() == KeyEvent.VK_ENTER ? new PlayScreen() : this;
	}
}
