package com.uvsq21805087.screens;

import com.uvsq21805087.Creature;
import com.uvsq21805087.Item;
import com.uvsq21805087.Tile;

public class LookScreen extends TargetBasedScreen {

	/**
	 * constructor
	 * 
	 * @param player the payer
	 * @param caption the caption
	 * @param sx coordinate x
	 * @param sy coordinate y
	 */
	public LookScreen(Creature player, String caption, int sx, int sy) {
		super(player, caption, sx, sy);
	}

	/**
	 * set the world coordinates
	 * 
	 * @param x coordinate x
	 * @param y coordinate y
	 * @param screenX screenX value
	 * @param screenY screenY value
	 * 
	 */
	public void enterWorldCoordinate(int x, int y, int screenX, int screenY) {
		Creature creature = player.creature(x, y, player.z);
		if (creature != null) {
			caption = creature.glyph() + " " + creature.name() + creature.details();
			return;
		}

		Item item = player.item(x, y, player.z);
		if (item != null) {
			caption = item.glyph() + " " + item.name() + item.details();
			return;
		}

		Tile tile = player.tile(x, y, player.z);
		caption = tile.glyph() + " " + tile.details();
	}
}