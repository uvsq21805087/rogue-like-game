package com.uvsq21805087.screens;

import com.uvsq21805087.Creature;
import com.uvsq21805087.Spell;

public class CastSpellScreen extends TargetBasedScreen {
	private Spell spell;

	public CastSpellScreen(Creature player, String caption, int sx, int sy, Spell spell) {
		super(player, caption, sx, sy);
		this.spell = spell;
	}

	/**
	 * it selects the world coordinates
	 */
	public void selectWorldCoordinate(int x, int y, int screenX, int screenY) {
		player.castSpell(spell, x, y);
	}
}
