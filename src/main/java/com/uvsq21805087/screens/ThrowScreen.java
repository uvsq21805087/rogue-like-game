package com.uvsq21805087.screens;

import com.uvsq21805087.Creature;
import com.uvsq21805087.Item;

public class ThrowScreen extends InventoryBasedScreen {
	private int sx;
	private int sy;

	/**
	 * constructor
	 * 
	 * @param player the player
	 * @param sx coordinate x
	 * @param sy coordinate y
	 */
	public ThrowScreen(Creature player, int sx, int sy) {
		super(player);
		this.sx = sx;
		this.sy = sy;
	}

	/**
	 * verb getter
	 */
	protected String getVerb() {
		return "throw";
	}

	/**
	 * verify if an item is acceptable
	 */
	protected boolean isAcceptable(Item item) {
		return true;
	}

	protected Screen use(Item item) {
		return new ThrowAtScreen(player, sx, sy, item);
	}
}
