package com.uvsq21805087.screens;

import com.uvsq21805087.Creature;
import com.uvsq21805087.Item;

public class ExamineScreen extends InventoryBasedScreen {

	/**
	 * constructor
	 * 
	 * @param player the player
	 */
	public ExamineScreen(Creature player) {
		super(player);
	}

	/**
	 * verb getter
	 */
	protected String getVerb() {
		return "examine";
	}

	/**
	 * verify if an item is acceptable
	 * 
	 * @param item
	 * @return true if it is acceptable
	 */
	protected boolean isAcceptable(Item item) {
		return true;
	}

	protected Screen use(Item item) {
		String article = "aeiou".contains(item.name().subSequence(0, 1)) ? "an " : "a ";
		player.notify("It's " + article + item.name() + "." + item.details());
		return null;
	}
}