package com.uvsq21805087.screens;

import com.uvsq21805087.Creature;
import com.uvsq21805087.Item;

public class EatScreen extends InventoryBasedScreen {

	/**
	 * constructor
	 * 
	 * @param player the player
	 */
	public EatScreen(Creature player) {
		super(player);
	}

	/**
	 * verb getter
	 */
	protected String getVerb() {
		return "eat";
	}

	/**
	 * verify if an item if accepted
	 * 
	 * @param item
	 */
	protected boolean isAcceptable(Item item) {
		return item.foodValue() != 0;
	}

	/**
	 * use the item by the player
	 * 
	 * @param item
	 * @return null
	 */
	protected Screen use(Item item) {
		player.eat(item);
		return null;
	}
}