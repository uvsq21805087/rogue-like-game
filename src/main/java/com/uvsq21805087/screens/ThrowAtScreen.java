package com.uvsq21805087.screens;

import com.uvsq21805087.Creature;
import com.uvsq21805087.Item;
import com.uvsq21805087.Line;
import com.uvsq21805087.Point;

public class ThrowAtScreen extends TargetBasedScreen {
	private Item item;

	/**
	 * consctructor
	 * 
	 * @param player the player
	 * @param sx x coordinate
	 * @param sy y coordinate
	 * @param item the item
	 */
	public ThrowAtScreen(Creature player, int sx, int sy, Item item) {
		super(player, "Throw " + item.name() + " at?", sx, sy);
		this.item = item;
	}

	public boolean isAcceptable(int x, int y) {
		if (!player.canSee(x, y, player.z))
			return false;

		for (Point p : new Line(player.x, player.y, x, y)) {
			if (!player.realTile(p.x, p.y, player.z).isGround())
				return false;
		}

		return true;
	}

	/**
	 * select the world coordinates
	 * 
	 * @param x coordinate x
	 * @param y coordinate y
	 * @param screenX screen coordinate x
	 * @param screenY screen coordiate y
	 */
	public void selectWorldCoordinate(int x, int y, int screenX, int screenY) {
		player.throwItem(item, x, y, player.z);
	}

}