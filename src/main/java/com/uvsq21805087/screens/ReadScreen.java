package com.uvsq21805087.screens;

import com.uvsq21805087.Creature;
import com.uvsq21805087.Item;

public class ReadScreen extends InventoryBasedScreen {

	private int sx;
	private int sy;

	/**
	 * constructor
	 * 
	 * @param player the player
	 * @param sx coordinate x
	 * @param sy coordinaye y
	 */
	public ReadScreen(Creature player, int sx, int sy) {
		super(player);
		this.sx = sx;
		this.sy = sy;
	}

	/**
	 * verb getter
	 */
	protected String getVerb() {
		return "read";
	}

	/**
	 * verify if an item is acceptable
	 * 
	 * @param item
	 */

	protected boolean isAcceptable(Item item) {
		return !item.writtenSpells().isEmpty();
	}

	protected Screen use(Item item) {
		return new ReadSpellScreen(player, sx, sy, item);
	}
}