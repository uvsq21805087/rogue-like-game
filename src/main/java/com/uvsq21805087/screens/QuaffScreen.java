package com.uvsq21805087.screens;

import com.uvsq21805087.Creature;
import com.uvsq21805087.Item;

public class QuaffScreen extends InventoryBasedScreen {

	/**
	 * constructor
	 * 
	 * @param player the player
	 */
	public QuaffScreen(Creature player) {
		super(player);
	}

	/**
	 * verb getter
	 */
	protected String getVerb() {
		return "quaff";
	}

	/**
	 * verify if the item is acceptable
	 * 
	 * @param item
	 * @return returns true if it is
	 */
	protected boolean isAcceptable(Item item) {
		return item.quaffEffect() != null;
	}

	protected Screen use(Item item) {
		player.quaff(item);
		return null;
	}

}