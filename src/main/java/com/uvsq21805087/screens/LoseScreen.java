package com.uvsq21805087.screens;

import java.awt.event.KeyEvent;
import asciiPanel.AsciiPanel;

public class LoseScreen implements Screen {

	/**
	 * indicate that the player lost
	 */
	public void displayOutput(AsciiPanel terminal) {
		terminal.write(".____      ________      _________ ___________ ._.", 5, 5);
		terminal.write("|    |     \\_____  \\    /   _____/ \\_   _____/ | |", 5, 6);
		terminal.write("|    |      /   |   \\   \\_____  \\   |    __)_  | |", 5, 7);
		terminal.write("|    |___  /    |    \\  /        \\  |        \\  \\|", 5, 8);
		terminal.write("|_______ \\ \\_______  / /_______  / /_______  /  __", 5, 9);
		terminal.write("        \\/         \\/          \\/          \\/   \\/", 5, 10);


		terminal.writeCenter("-- press [enter] to restart --", 22);
	}

	public Screen respondToUserInput(KeyEvent key) {
		return key.getKeyCode() == KeyEvent.VK_ENTER ? new PlayScreen() : this;
	}
}