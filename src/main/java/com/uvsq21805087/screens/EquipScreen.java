package com.uvsq21805087.screens;

import com.uvsq21805087.Creature;
import com.uvsq21805087.Item;

public class EquipScreen extends InventoryBasedScreen {

	/**
	 * constructor
	 * 
	 * @param player the player
	 */
	public EquipScreen(Creature player) {
		super(player);
	}

	/**
	 * verb getter
	 */
	protected String getVerb() {
		return "wear or wield";
	}

	/**
	 * verify if an item is acceptable
	 * 
	 * @param item
	 * @return true if it is acceptable
	 */
	protected boolean isAcceptable(Item item) {
		return item.attackValue() > 0 || item.defenseValue() > 0;
	}

	protected Screen use(Item item) {
		player.equip(item);
		return null;
	}
}