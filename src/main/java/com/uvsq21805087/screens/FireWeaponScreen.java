package com.uvsq21805087.screens;

import com.uvsq21805087.Creature;
import com.uvsq21805087.Line;
import com.uvsq21805087.Point;

public class FireWeaponScreen extends TargetBasedScreen {

	/**
	 * constructor
	 * 
	 * @param player the player
	 * @param sx     coordinate x
	 * @param sy     coordinate y
	 */
	public FireWeaponScreen(Creature player, int sx, int sy) {
		super(player, "Fire " + player.weapon().name() + " at?", sx, sy);
	}

	/**
	 * verify if an item is acceptable in coordinates x,y
	 * 
	 * @param x coordinate x
	 * @param y coordinate y
	 * @return true if it is acceptable
	 */
	public boolean isAcceptable(int x, int y) {
		if (!player.canSee(x, y, player.z))
			return false;

		for (Point p : new Line(player.x, player.y, x, y)) {
			if (!player.realTile(p.x, p.y, player.z).isGround())
				return false;
		}

		return true;
	}

	public void selectWorldCoordinate(int x, int y, int screenX, int screenY) {
		Creature other = player.creature(x, y, player.z);

		if (other == null)
			player.notify("There's no one there to fire at.");
		else
			player.rangedWeaponAttack(other);
	}
}