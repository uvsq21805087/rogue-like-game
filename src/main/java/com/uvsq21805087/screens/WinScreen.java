package com.uvsq21805087.screens;

import java.awt.event.KeyEvent;
import asciiPanel.AsciiPanel;

public class WinScreen implements Screen {

	/**
	 * display the Win message
	 */
	public void displayOutput(AsciiPanel terminal) {
		terminal.write(" __          __  _____   _   _   _ ", 5, 5);
		terminal.write(" \\ \\        / / |_   _| | \\ | | | |", 5, 6);
		terminal.write("  \\ \\  /\\  / /    | |   |  \\| | | |", 5, 7);
		terminal.write("   \\ \\/  \\/ /     | |   | . ` | | |", 5, 8);
		terminal.write("    \\  /\\  /     _| |_  | |\\  | |_|", 5, 9);
		terminal.write("     \\/  \\/     |_____| |_| \\_| (_)", 5, 10);
		terminal.write("                                   ", 5, 11);
		terminal.writeCenter("-- press [enter] to restart --", 22);
	}

	public Screen respondToUserInput(KeyEvent key) {
		return key.getKeyCode() == KeyEvent.VK_ENTER ? new PlayScreen() : this;
	}
}