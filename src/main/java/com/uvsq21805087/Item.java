package com.uvsq21805087;

import java.awt.Color;
import java.util.ArrayList;

public class Item {
	private char glyph;

	/**
	 * returns the glyph
	 * 
	 * @return the glyph
	 */
	public char glyph() {
		return glyph;
	}

	private Color color;

	/**
	 * gives the color
	 * 
	 * @return the item color
	 */
	public Color color() {
		return color;
	}

	private String name;

	public String name() {
		return name;
	}

	private int foodValue;

	/**
	 * gives the foodValue
	 * 
	 * @return the foodvalue
	 */
	public int foodValue() {
		return foodValue;
	}

	public void modifyFoodValue(int amount) {
		foodValue += amount;
	}

	private int attackValue;

	public int attackValue() {
		return attackValue;
	}

	public void modifyAttackValue(int amount) {
		attackValue += amount;
	}

	private int defenseValue;

	public int defenseValue() {
		return defenseValue;
	}

	public void modifyDefenseValue(int amount) {
		defenseValue += amount;
	}

	private int thrownAttackValue;

	public int thrownAttackValue() {
		return thrownAttackValue;
	}

	/**
	 * increase the attackValue with the amount value
	 * 
	 * @param amount the amount value
	 */
	public void modifyThrownAttackValue(int amount) {
		thrownAttackValue += amount;
	}

	private int rangedAttackValue;

	public int rangedAttackValue() {
		return rangedAttackValue;
	}

	public void modifyRangedAttackValue(int amount) {
		rangedAttackValue += amount;
	}

	private Effect quaffEffect;

	/**
	 * returns the effect
	 * 
	 * @return the quaffEffect
	 */
	public Effect quaffEffect() {
		return quaffEffect;
	}

	public void setQuaffEffect(Effect effect) {
		this.quaffEffect = effect;
	}

	private ArrayList<Spell> writtenSpells;

	public ArrayList<Spell> writtenSpells() {
		return writtenSpells;
	}

	public void addWrittenSpell(String name, int manaCost, Effect effect) {
		writtenSpells.add(new Spell(name, manaCost, effect));
	}

	private String appearance;

	/**
	 * gives the apparence of the item
	 * 
	 * @return apparence
	 */
	public String appearance() {
		if (appearance == null)
			return name;

		return appearance;
	}

	public Item(char glyph, Color color, String name, String appearance) {
		this.glyph = glyph;
		this.color = color;
		this.name = name;
		this.appearance = appearance == null ? name : appearance;
		this.thrownAttackValue = 1;
		this.writtenSpells = new ArrayList<Spell>();
	}

	/**
	 * gives more details about the item
	 * 
	 * @return a short description
	 */
	public String details() {
		String details = "";

		if (attackValue != 0)
			details += "  attack:" + attackValue;

		if (thrownAttackValue != 1)
			details += "  thrown:" + thrownAttackValue;

		if (rangedAttackValue > 0)
			details += "  ranged:" + rangedAttackValue;

		if (defenseValue != 0)
			details += "  defense:" + defenseValue;

		if (foodValue != 0)
			details += "  food:" + foodValue;

		return details;
	}
}