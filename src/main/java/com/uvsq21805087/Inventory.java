package com.uvsq21805087;

public class Inventory {

	private Item[] items;

	public Item[] getItems() {
		return items;
	}

	public Item get(int i) {
		return items[i];
	}

	/**
	 * constructor
	 * 
	 * @param max the max indice value
	 */
	public Inventory(int max) {
		items = new Item[max];
	}

	/**
	 * add new item
	 * 
	 * @param item the item
	 */
	public void add(Item item) {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null) {
				items[i] = item;
				break;
			}
		}
	}

	/**
	 * remove item
	 * 
	 * @param item the item to remove
	 */
	public void remove(Item item) {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == item) {
				items[i] = null;
				return;
			}
		}
	}

	/**
	 * verify is the items table is full
	 * 
	 * @return true if it is
	 */
	public boolean isFull() {
		int size = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null)
				size++;
		}
		return size == items.length;
	}

	/**
	 * verify if an item is ocntains in the items table
	 * 
	 * @param item the atem
	 * @return true if it is contained
	 */
	public boolean contains(Item item) {
		for (Item i : items) {
			if (i == item)
				return true;
		}
		return false;
	}
}