package com.uvsq21805087;

import java.awt.Color;
import java.util.ArrayList;

public class Creature {
	private World world;

	public int x;
	public int y;
	public int z;

	private char glyph;

	/**
	 * 
	 * @return the used glyph
	 */
	public char glyph() {
		return glyph;
	}

	private Color color;

	/**
	 * color
	 * 
	 * @return color
	 */
	public Color color() {
		return color;
	}

	private CreatureAi ai;

	/**
	 * 
	 * @param ai the creature
	 */
	public void setCreatureAi(CreatureAi ai) {
		this.ai = ai;
	}

	private int maxHp;

	/**
	 * gives the max value of health
	 * 
	 * @return the maximum value of health
	 */
	public int maxHp() {
		return maxHp;
	}

	/**
	 * increase the level of health
	 * 
	 * @param amount the amount by which the level of health is incremented
	 */
	public void modifyMaxHp(int amount) {
		maxHp += amount;
	}

	private int hp;

	/**
	 * 
	 * @return the actual value of health
	 */
	public int hp() {
		return hp;
	}

	private int attackValue;

	/**
	 * increment attack power
	 * 
	 * @param value the amount by which the attackValue is incremented
	 */
	public void modifyAttackValue(int value) {
		attackValue += value;
	}

	/**
	 * gives the attack Value
	 * 
	 * @return the value of the attack
	 */
	public int attackValue() {
		return attackValue + (weapon == null ? 0 : weapon.attackValue()) + (armor == null ? 0 : armor.attackValue());
	}

	private int defenseValue;

	/**
	 * increment the defenseValue
	 * 
	 * @param value the amount by which the defenseValue is incremented
	 */
	public void modifyDefenseValue(int value) {
		defenseValue += value;
	}

	/**
	 * gives the defenseValue
	 * 
	 * @return the defenseValue
	 */
	public int defenseValue() {
		return defenseValue + (weapon == null ? 0 : weapon.defenseValue()) + (armor == null ? 0 : armor.defenseValue());
	}

	private int visionRadius;

	/**
	 * increase the visionRaduis
	 * 
	 * @param value the amount by which the visionRaduis is incremented
	 */
	public void modifyVisionRadius(int value) {
		visionRadius += value;
	}

	/**
	 * gives the value of visionRaduis
	 * 
	 * @return visionRaduis
	 */
	public int visionRadius() {
		return visionRadius;
	}

	private String name;

	/**
	 * gives the creature name
	 * 
	 * @return name
	 */
	public String name() {
		return name;
	}

	private Inventory inventory;

	/**
	 * gives inventory
	 * 
	 * @return inventory
	 */
	public Inventory inventory() {
		return inventory;
	}

	private int maxFood;

	/**
	 * gives the maxFood value that can be eated
	 * 
	 * @return maxFood
	 */
	public int maxFood() {
		return maxFood;
	}

	private int food;

	/**
	 * gives the food value
	 * 
	 * @return food
	 */
	public int food() {
		return food;
	}

	private Item weapon;

	/**
	 * gives the weapon item
	 * 
	 * @return weapon
	 */
	public Item weapon() {
		return weapon;
	}

	private Item armor;

	/**
	 * give the armor item
	 * 
	 * @return armor
	 */
	public Item armor() {
		return armor;
	}

	private int xp;

	/**
	 * gives the experience
	 * 
	 * @return experience
	 */
	public int xp() {
		return xp;
	}

	/**
	 * increment experience with the amount value passes levels generate messages
	 * modify hp value
	 * 
	 * @param amount the amount value
	 */
	public void modifyXp(int amount) {
		xp += amount;

		notify("You %s %d xp.", amount < 0 ? "lose" : "gain", amount);

		while (xp > (int) (Math.pow(level, 1.75) * 25)) {
			level++;
			doAction("advance to level %d", level);
			ai.onGainLevel();
			modifyHp(level * 2, "Died from having a negative level?");
		}
	}

	private int level;

	/**
	 * gives the level number
	 * 
	 * @return lever
	 */
	public int level() {
		return level;
	}

	private int regenHpCooldown;
	private int regenHpPer1000;

	/**
	 * increment regenHpPer1000 with amount value
	 * 
	 * @param amount the amount value
	 */
	public void modifyRegenHpPer1000(int amount) {
		regenHpPer1000 += amount;
	}

	private ArrayList<Effect> effects;

	/**
	 * gives a list of effects
	 * 
	 * @return effects list
	 */
	public ArrayList<Effect> effects() {
		return effects;
	}

	private int maxMana;

	/**
	 * gives maxMana
	 * 
	 * @return maxMana
	 */
	public int maxMana() {
		return maxMana;
	}

	/**
	 * increment maxMana with the amount value
	 * 
	 * @param amount the amount value
	 */
	public void modifyMaxMana(int amount) {
		maxMana += amount;
	}

	private int mana;

	/**
	 * gives mana
	 * 
	 * @return mana
	 */
	public int mana() {
		return mana;
	}

	/**
	 * modify mana value
	 * 
	 * @param amount the amount of modification
	 */
	public void modifyMana(int amount) {
		mana = Math.max(0, Math.min(mana + amount, maxMana));
	}

	private int regenManaCooldown;
	private int regenManaPer1000;

	/**
	 * increment regenManaPer1000 with the amount value
	 * 
	 * @param amount the amount value
	 */
	public void modifyRegenManaPer1000(int amount) {
		regenManaPer1000 += amount;
	}

	private String causeOfDeath;

	/**
	 * gives the cause of death
	 * 
	 * @return causeOfDeath
	 */
	public String causeOfDeath() {
		return causeOfDeath;
	}

	/**
	 * constructor
	 * 
	 * @param world   specify the world
	 * @param glyph   the glyph used
	 * @param color   the color
	 * @param name    the creature name
	 * @param maxHp   the max health value
	 * @param attack  the attack value
	 * @param defense the defense value
	 */
	public Creature(World world, char glyph, Color color, String name, int maxHp, int attack, int defense) {
		this.world = world;
		this.glyph = glyph;
		this.color = color;
		this.maxHp = maxHp;
		this.hp = maxHp;
		this.attackValue = attack;
		this.defenseValue = defense;
		this.visionRadius = 9;
		this.name = name;
		this.inventory = new Inventory(20);
		this.maxFood = 1000;
		this.food = maxFood / 3 * 2;
		this.level = 1;
		this.regenHpPer1000 = 10;
		this.effects = new ArrayList<Effect>();
		this.maxMana = 5;
		this.mana = maxMana;
		this.regenManaPer1000 = 20;
		this.ai = new CreatureAi(this);
	}

	/**
	 * use the mooving coordinates generate messages modify food
	 * 
	 * @param mx x coordinate
	 * @param my y coordinate
	 * @param mz z coordinate
	 */
	public void moveBy(int mx, int my, int mz) {
		if (mx == 0 && my == 0 && mz == 0)
			return;

		Tile tile = world.tile(x + mx, y + my, z + mz);

		if (mz == -1) {
			if (tile == Tile.STAIRS_DOWN) {
				doAction("walk up the stairs to level %d", z + mz + 1);
			} else {
				doAction("try to go up but are stopped by the cave ceiling");
				return;
			}
		} else if (mz == 1) {
			if (tile == Tile.STAIRS_UP) {
				doAction("walk down the stairs to level %d", z + mz + 1);
			} else {
				doAction("try to go down but are stopped by the cave floor");
				return;
			}
		}

		Creature other = world.creature(x + mx, y + my, z + mz);

		modifyFood(-1);

		if (other == null)
			ai.onEnter(x + mx, y + my, z + mz, tile);
		else
			meleeAttack(other);
	}

	/**
	 * attacks the other creature generate message
	 * 
	 * @param other the other creature
	 */
	public void meleeAttack(Creature other) {
		commonAttack(other, attackValue(), "attack the %s for %d damage", other.name);
	}

	/**
	 * attacks the other creature
	 * 
	 * @param item  the item used
	 * @param other the other creature
	 */
	private void throwAttack(Item item, Creature other) {
		commonAttack(other, attackValue / 2 + item.thrownAttackValue(), "throw a %s at the %s for %d damage",
				nameOf(item), other.name);
		other.addEffect(item.quaffEffect());
	}

	/**
	 * attack the other creature with weapon
	 * 
	 * @param other the other creature
	 */
	public void rangedWeaponAttack(Creature other) {
		commonAttack(other, attackValue / 2 + weapon.rangedAttackValue(), "fire a %s at the %s for %d damage",
				nameOf(weapon), other.name);
	}

	/**
	 * attack the other creature with attack value by the action: action
	 * 
	 * @param other  the other creature
	 * @param attack the attack value
	 * @param action the action
	 * @param params table of parameters
	 */
	private void commonAttack(Creature other, int attack, String action, Object... params) {
		modifyFood(-2);

		int amount = Math.max(0, attack - other.defenseValue());

		amount = (int) (Math.random() * amount) + 1;

		Object[] params2 = new Object[params.length + 1];
		for (int i = 0; i < params.length; i++) {
			params2[i] = params[i];
		}
		params2[params2.length - 1] = amount;

		doAction(action, params2);

		other.modifyHp(-amount, "Killed by a " + name);

		if (other.hp < 1)
			gainXp(other);
	}

	/**
	 * calculate the amount value modify Xp
	 * 
	 * @param other the other creature
	 */
	public void gainXp(Creature other) {
		int amount = other.maxHp + other.attackValue() + other.defenseValue() - level;

		if (amount > 0)
			modifyXp(amount);
	}

	/**
	 * modify Hp value generate message
	 * 
	 * @param amount       the amount value
	 * @param causeOfDeath the cause of death
	 */
	public void modifyHp(int amount, String causeOfDeath) {
		hp += amount;
		this.causeOfDeath = causeOfDeath;

		if (hp > maxHp) {
			hp = maxHp;
		} else if (hp < 1) {
			doAction("die");
			leaveCorpse();
			world.remove(this);
		}
	}

	/**
	 * create % item when a creature dies
	 */
	private void leaveCorpse() {
		Item corpse = new Item('%', color, name + " corpse", null);
		corpse.modifyFoodValue(maxHp * 5);
		world.addAtEmptySpace(corpse, x, y, z);
		for (Item item : inventory.getItems()) {
			if (item != null)
				drop(item);
		}
	}

	/**
	 * dig into wall
	 * 
	 * @param wx the x coordinate
	 * @param wy the y coordinate
	 * @param wz the z coordinate
	 */
	public void dig(int wx, int wy, int wz) {
		modifyFood(-10);
		world.dig(wx, wy, wz);
		doAction("dig");
	}

	/**
	 * food update generate health generate mana update effects apply the update on
	 * ai creature
	 */
	public void update() {
		modifyFood(-1);
		regenerateHealth();
		regenerateMana();
		updateEffects();
		ai.onUpdate();
	}

	/**
	 * update effects
	 */
	private void updateEffects() {
		ArrayList<Effect> done = new ArrayList<Effect>();

		for (Effect effect : effects) {
			effect.update(this);
			if (effect.isDone()) {
				effect.end(this);
				done.add(effect);
			}
		}

		effects.removeAll(done);
	}

	/**
	 * health regeneration
	 */
	private void regenerateHealth() {
		regenHpCooldown -= regenHpPer1000;
		if (regenHpCooldown < 0) {
			if (hp < maxHp) {
				modifyHp(1, "Died from regenerating health?");
				modifyFood(-1);
			}
			regenHpCooldown += 1000;
		}
	}

	/**
	 * mana regeneration
	 */
	private void regenerateMana() {
		regenManaCooldown -= regenManaPer1000;
		if (regenManaCooldown < 0) {
			if (mana < maxMana) {
				modifyMana(1);
				modifyFood(-1);
			}
			regenManaCooldown += 1000;
		}
	}

	/**
	 * verifie if creature can enter
	 * 
	 * @param wx the x coordinate
	 * @param wy the y coordinate
	 * @param wz the z coordinate
	 * @return true if creature can enter
	 */
	public boolean canEnter(int wx, int wy, int wz) {
		return world.tile(wx, wy, wz).isGround() && world.creature(wx, wy, wz) == null;
	}

	/**
	 * generate messages
	 * 
	 * @param message the message
	 * @param params  the parameters value
	 */
	public void notify(String message, Object... params) {
		ai.onNotify(String.format(message, params));
	}

	/**
	 * the action to do generate messages
	 * 
	 * @param message the message
	 * @param params  the parameters value
	 */
	public void doAction(String message, Object... params) {
		for (Creature other : getCreaturesWhoSeeMe()) {
			if (other == this) {
				other.notify("You " + message + ".", params);
			} else {
				other.notify(String.format("The %s %s.", name, makeSecondPerson(message)), params);
			}
		}
	}

	/**
	 * the action done by the item informs other creature about the item generate a
	 * message
	 * 
	 * @param item    the item
	 * @param message the message
	 * @param params  the parameters value
	 */
	public void doAction(Item item, String message, Object... params) {
		if (hp < 1)
			return;

		for (Creature other : getCreaturesWhoSeeMe()) {
			if (other == this) {
				other.notify("You " + message + ".", params);
			} else {
				other.notify(String.format("The %s %s.", name, makeSecondPerson(message)), params);
			}
			other.learnName(item);
		}
	}

	/**
	 * gives a list of other creatures that can see me
	 * 
	 * @return a list of creatures
	 */
	private ArrayList<Creature> getCreaturesWhoSeeMe() {
		ArrayList<Creature> others = new ArrayList<Creature>();
		int r = 9;
		for (int ox = -r; ox < r + 1; ox++) {
			for (int oy = -r; oy < r + 1; oy++) {
				if (ox * ox + oy * oy > r * r)
					continue;

				Creature other = world.creature(x + ox, y + oy, z);

				if (other == null)
					continue;

				others.add(other);
			}
		}
		return others;
	}

	/**
	 * make second person
	 * 
	 * @param text
	 * @return a string
	 */
	private String makeSecondPerson(String text) {
		String[] words = text.split(" ");
		words[0] = words[0] + "s";

		StringBuilder builder = new StringBuilder();
		for (String word : words) {
			builder.append(" ");
			builder.append(word);
		}

		return builder.toString().trim();
	}

	/**
	 * verifie if creature can see
	 * 
	 * @param wx the x coordinate
	 * @param wy the y coordinate
	 * @param wz the z coordinate
	 * @return true if creature can see
	 */
	public boolean canSee(int wx, int wy, int wz) {
		return (detectCreatures > 0 && world.creature(wx, wy, wz) != null || ai.canSee(wx, wy, wz));
	}

	/**
	 * give the real tile
	 * 
	 * @param wx coordinate x
	 * @param wy coordinate y
	 * @param wz coordinate z
	 * @return tile the tile
	 */
	public Tile realTile(int wx, int wy, int wz) {
		return world.tile(wx, wy, wz);
	}

	/**
	 * tile that he see, or the previous tile
	 * 
	 * @param wx coordinate x
	 * @param wy coordiate y
	 * @param wz coordinate z
	 * @return tile the tile 
	 */
	public Tile tile(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return world.tile(wx, wy, wz);
		else
			return ai.rememberedTile(wx, wy, wz);
	}

	/**
	 * give the creature that he see returns null f he see nothing
	 * 
	 * @param wx the coordinate x
	 * @param wy the coordinate y
	 * @param wz the coordinate z
	 * @return creature that he see
	 */
	public Creature creature(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return world.creature(wx, wy, wz);
		else
			return null;
	}

	/**
	 * pick up items generate messages fill in the inventory
	 */
	public void pickup() {
		Item item = world.item(x, y, z);

		if (inventory.isFull() || item == null) {
			doAction("grab at the ground");
		} else {
			doAction("pickup a %s", nameOf(item));
			world.remove(x, y, z);
			inventory.add(item);
		}
	}

	/**
	 * drops the item remove it from the inventory generates a message
	 * 
	 * @param item the item to drop
	 */
	public void drop(Item item) {
		if (world.addAtEmptySpace(item, x, y, z)) {
			doAction("drop a " + nameOf(item));
			inventory.remove(item);
			unequip(item);
		} else {
			notify("There's nowhere to drop the %s.", nameOf(item));
		}
	}

	/**
	 * modify food value by incrementing it with the amount value modify maxFood
	 * value
	 * 
	 * @param amount the amount value
	 */
	public void modifyFood(int amount) {
		food += amount;

		if (food > maxFood) {
			maxFood = (maxFood + food) / 2;
			food = maxFood;
			notify("You can't belive your stomach can hold that much!");
			modifyHp(-1, "Killed by overeating.");
		} else if (food < 1 && isPlayer()) {
			modifyHp(-1000, "Starved to death.");
		}
	}

	/**
	 * verifie if it's a player
	 * 
	 * @return true if it's a player
	 */
	public boolean isPlayer() {
		return glyph == '@';
	}

	/**
	 * eats item
	 * 
	 * @param item the item to eat
	 */
	public void eat(Item item) {
		doAction("eat a " + nameOf(item));
		consume(item);
	}

	/**
	 * quaff an item
	 * 
	 * @param item the item to quaff
	 */
	public void quaff(Item item) {
		doAction("quaff a " + nameOf(item));
		consume(item);
	}

	/**
	 * consume an item add effects modify food value generate a message
	 * 
	 * @param item the item to consume
	 */
	private void consume(Item item) {
		if (item.foodValue() < 0)
			notify("Gross!");

		addEffect(item.quaffEffect());

		modifyFood(item.foodValue());
		getRidOf(item);
	}

	/**
	 * add effect to the list
	 * 
	 * @param effect the effect to add
	 */
	private void addEffect(Effect effect) {
		if (effect == null)
			return;

		effect.start(this);
		effects.add(effect);
	}

	/**
	 * removes the tem from the inventory unequips the item
	 * 
	 * @param item the item
	 */
	private void getRidOf(Item item) {
		inventory.remove(item);
		unequip(item);
	}

	/**
	 * put the item in an empty space
	 * 
	 * @param item the item to put
	 * @param wx   the coordinate x
	 * @param wy   the coordnate y
	 * @param wz   the coordinate z
	 */
	private void putAt(Item item, int wx, int wy, int wz) {
		inventory.remove(item);
		unequip(item);
		world.addAtEmptySpace(item, wx, wy, wz);
	}

	/**
	 * unequip the item
	 * 
	 * @param item the item to unequip
	 */
	public void unequip(Item item) {
		if (item == null)
			return;

		if (item == armor) {
			if (hp > 0)
				doAction("remove a " + nameOf(item));
			armor = null;
		} else if (item == weapon) {
			if (hp > 0)
				doAction("put away a " + nameOf(item));
			weapon = null;
		}
	}

	/**
	 * equip an item generate notifications
	 * 
	 * @param item the item to equip
	 */
	public void equip(Item item) {
		if (!inventory.contains(item)) {
			if (inventory.isFull()) {
				notify("Can't equip %s since you're holding too much stuff.", nameOf(item));
				return;
			} else {
				world.remove(item);
				inventory.add(item);
			}
		}

		if (item.attackValue() == 0 && item.rangedAttackValue() == 0 && item.defenseValue() == 0)
			return;

		if (item.attackValue() + item.rangedAttackValue() >= item.defenseValue()) {
			unequip(weapon);
			doAction("wield a " + nameOf(item));
			weapon = item;
		} else {
			unequip(armor);
			doAction("put on a " + nameOf(item));
			armor = item;
		}
	}

	/**
	 * gives the item that he see in the position (x,y,z)
	 * 
	 * @param wx the coordinate x
	 * @param wy the coordinate y
	 * @param wz the coordinate z
	 * @return the item he see, or null
	 */
	public Item item(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return world.item(wx, wy, wz);
		else
			return null;
	}

	/**
	 * gives details
	 * 
	 * @return some details about level, attack value and defense value
	 */
	public String details() {
		return String.format("  level:%d  attack:%d  defense:%d  hp:%d", level, attackValue(), defenseValue(), hp);
	}

	/**
	 * throw item
	 * 
	 * @param item the item to throw
	 * @param wx   coordinate x
	 * @param wy   coordinate y
	 * @param wz   coordinate z
	 */
	public void throwItem(Item item, int wx, int wy, int wz) {
		Point end = new Point(x, y, 0);

		for (Point p : new Line(x, y, wx, wy)) {
			if (!realTile(p.x, p.y, z).isGround())
				break;
			end = p;
		}

		wx = end.x;
		wy = end.y;

		Creature c = creature(wx, wy, wz);

		if (c != null)
			throwAttack(item, c);
		else
			doAction("throw a %s", nameOf(item));

		if (item.quaffEffect() != null && c != null)
			getRidOf(item);
		else
			putAt(item, wx, wy, wz);
	}

	/**
	 * add other creature to the world
	 * 
	 * @param other the other creatur
	 */
	public void summon(Creature other) {
		world.add(other);
	}

	private int detectCreatures;

	/**
	 * increase detectCreatures with the amount value
	 * 
	 * @param amount the amount value
	 */
	public void modifyDetectCreatures(int amount) {
		detectCreatures += amount;
	}

	/**
	 * cast spell
	 * 
	 * @param spell the used spell
	 * @param x2    coordinate x
	 * @param y2    coordinate y
	 */
	public void castSpell(Spell spell, int x2, int y2) {
		Creature other = creature(x2, y2, z);

		if (spell.manaCost() > mana) {
			doAction("point and mumble but nothing happens");
			return;
		} else if (other == null) {
			doAction("point and mumble at nothing");
			return;
		}

		other.addEffect(spell.effect());
		modifyMana(-spell.manaCost());
	}

	/**
	 * return the name of the item
	 * 
	 * @param item the item
	 * @return the item name
	 */
	public String nameOf(Item item) {
		return ai.getName(item);
	}

	/**
	 * set the item name
	 * 
	 * @param item the item
	 */
	public void learnName(Item item) {
		notify("The " + item.appearance() + " is a " + item.name() + "!");
		ai.setName(item, item.name());
	}
}