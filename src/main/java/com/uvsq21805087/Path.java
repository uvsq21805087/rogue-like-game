package com.uvsq21805087;

import java.util.ArrayList;

public class Path {

	private static PathFinder pf = new PathFinder();

	private ArrayList<Point> points;

	public ArrayList<Point> points() {
		return points;
	}

	public Path(Creature creature, int x, int y) {
		points = pf.findPath(creature, new Point(creature.x, creature.y, creature.z), new Point(x, y, creature.z), 300);
	}
}