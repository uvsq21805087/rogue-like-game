package com.uvsq21805087;

import java.util.ArrayList;

public class ZombieAi extends CreatureAi {
	private Creature player;

	/**
	 * constructor
	 * 
	 * @param creature the creature
	 * @param player   the player
	 */
	public ZombieAi(Creature creature, Creature player) {
		super(creature);
		this.player = player;
	}

	/**
	 * if the zoombie can see, it hunts the player
	 */
	public void onUpdate() {
		if (Math.random() < 0.2)
			return;

		if (creature.canSee(player.x, player.y, player.z))
			hunt(player);
		else
			wander();
	}

	/**
	 * hunt the target creature
	 * 
	 * @param target the target
	 */
	public void hunt(Creature target) {
		ArrayList<Point> points = (ArrayList<Point>) new Path(creature, target.x, target.y).points();

		int mx = points.get(0).x - creature.x;
		int my = points.get(0).y - creature.y;

		creature.moveBy(mx, my, 0);
	}
}
