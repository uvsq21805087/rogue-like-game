package com.uvsq21805087;

public class Effect {
	protected int duration;

	/**
	 * verify if it's already done
	 * 
	 * @return true if it's done
	 */
	public boolean isDone() {
		return duration < 1;
	}

	/**
	 * constructor 1
	 * 
	 * @param duration the initial duration
	 */
	public Effect(int duration) {
		this.duration = duration;
	}

	/**
	 * construtor 2
	 * 
	 * @param other the other effect
	 */
	public Effect(Effect other) {
		this.duration = other.duration;
	}

	/**
	 * decrease duration
	 * 
	 * @param creature the creature
	 */
	public void update(Creature creature) {
		duration--;
	}

	public void start(Creature creature) {

	}

	public void end(Creature creature) {

	}
}
