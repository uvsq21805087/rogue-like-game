## Description du jeu
* Ce jeu est la première version complet du projet Rogue Like Game v1.0
* A chaque début de partie un seul joueur est généré aléatoirement.
* Ensuite, un certain nombre de monstres sont générés aléatoirement dans l'univers du jeu, ainsi que des objet ramassable par le joueur.
* Les statistiques du joueur sont affichées en bas de l'écran.
* pour gagner une partie il faut accumuler de l'éxperience.
* Pour gagner de l'éxperience, il faut tuer un maximum de monstres sont que le joueur meurt en premier.
* Pour chaque niveau gagnée, le niveau suivant est débloqué.

## Installation du jeu
* Télécharger l'archive du jeu
* L'exécuter avec la commande java -jar <nom de l'archive>

## Commandes de base
* HAUT, BAS, GAUCHE, DROITE permettent de faire naviguer le joueur dans le monde
* [g] ou [,] pour ramasser un objet
* [d] pour jeter un objet par terre
* [t] pour jeter un objet contre une créature
* [x] pour examiner les statistique de chaque créature
* [;] pour regarder autour de soi-même
* [?] pour avoir de l'aide
* [s] pour sauvegarder la partie

## Contraintes techniques
* Ce jeu utilise Java 8, par conséquence il est indisponsable d'avoir une version égale ou ultérieur.

## Références
* Article Wikipedia [Rogue-like](https://fr.wikipedia.org/wiki/Rogue-like)
* Le tutoriel [roguelike tutorial](http://trystans.blogspot.fr/2016/01/roguelike-tutorial-00-table-of-contents.html)
* La bibliothèque [JAnsi](http://fusesource.github.io/jansi/) pour gérer la couleur dans un terminal

## Perspective
* La version 1.1 va améliorer la suavegarde du jeu, ainsi que le passage de niveau à un autre